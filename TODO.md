### TODO files
- [X]  semantic visioning 
   - [ ] [automating-semantic-versioning](https://hodgkins.io/automating-semantic-versioning#plugin---github)
   - [ ] [egg head ](https://egghead.io/lessons/javascript-automating-releases-with-semantic-release)
   - [ ] [Sematic release github](https://github.com/semantic-release/semantic-release)
   ```shell
     npm install -g semantic-release-cli
    ```
   - semantic versioning  FAQ https://semver.org/#faq
   - [semantic versioning angular](https://blog.angularindepth.com/the-angular-devops-series-semantically-release-your-angular-library-7d78afb4c845)
   - [what is scope in versioning ](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#scope)
- [X]  compodoc for documentation
- [X]  docker-compose setup/server lite setup 
- [ ]  basic ngsx store
- [ ]  materials 
- [ ]  map lib setup
- [ ]  cesium setup in map lib 
- [ ]  adding tensorflow js with the application 
   - [ ] [6.1: Introduction to TensorFlow.js - Intelligence and Learning](https://www.youtube.com/watch?v=Qt3ZABW5lD0&t=0s&index=2&list=PLRqwX-V7Uu6YIeVA3dNxbR9PYj4wV31oQ)
