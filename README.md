# Ibis
[![Build Status](https://travis-ci.com/arvindshekar07/ibis.svg?branch=master)](https://travis-ci.com/arvindshekar07/ibis)
[![codecov](https://codecov.io/gh/arvindshekar07/ibis/branch/master/graph/badge.svg)](https://codecov.io/gh/arvindshekar07/ibis)
[![Waffle.io - Columns and their card count](https://badge.waffle.io/arvindshekar07/ibis.svg?columns=all)](https://waffle.io/arvindshekar07/ibis)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

### references
https://blog.greenkeeper.io/introduction-to-semver-d272990c44f2
https://semantic-release.gitbook.io/semantic-release/usage/plugins

[ Too good a resoucer](https://hodgkins.io/automating-semantic-versioning)
 
